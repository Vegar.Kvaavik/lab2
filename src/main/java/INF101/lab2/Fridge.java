package INF101.lab2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> itemsInFridge = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge.size() < totalSize()) {
            itemsInFridge.add(item);
            return true;
        } else {
            System.out.println("Unable to place " + item + " in fridge. Not enough space");
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!itemsInFridge.contains(item)) {
            throw new NoSuchElementException();
        } else {
            itemsInFridge.remove(item);
        }

    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // Create an empty list that should contain expired items
        ArrayList<FridgeItem> expired = new ArrayList<>();

        // Loop through all items in fridge and add expired items to the 'expired'-list
        for (FridgeItem item : itemsInFridge) {
            if (item.hasExpired()) {
                expired.add(item);
            }
        }

        // Remove all expired items from the fridge
        itemsInFridge.removeAll(expired);
        return expired;
    }
}
